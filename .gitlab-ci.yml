
stages:
  #  - formatting
  - test_source
  - build_container
  - test_container
  - merge_request_to_infra

services:
  - docker:dind

.merge_main:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "push"
    - when: on_success

.request_main:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "merge_request_event"

variables:
  CI_DEBUG_TRACE: "false"

test_source:
  stage: test_source
  image: ubuntu:20.04
  rules:
    - when: always
  script:
    - apt update
    # run tests
    - DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends tzdata
    - apt install -y curl build-essential
    - curl -OL https://golang.org/dl/go1.17.7.linux-amd64.tar.gz
    - tar -C /usr/local -xvf go1.17.7.linux-amd64.tar.gz
    - export PATH=$PATH:/usr/local/go/bin
    - go version
    - go install
    - go test ./...
    # run gosec scan
    - curl -sfL https://raw.githubusercontent.com/securego/gosec/master/install.sh | sh -s v2.9.6
    - ./bin/gosec ./... | tee gosec-output.txt
  artifacts:
    paths:
      - gosec-output.txt

build_container:
  stage: build_container
  image: docker:20
  rules:
    - when: always
  script:
    - echo -n $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - >
      docker build
      --pull
      --build-arg http_proxy=$http_proxy
      --build-arg https_proxy=$https_proxy
      --build-arg no_proxy=$no_proxy
      --cache-from $CI_REGISTRY_IMAGE:latest
      --label "org.opencontainers.image.title=$CI_PROJECT_TITLE"
      --label "org.opencontainers.image.url=$CI_PROJECT_URL"
      --label "org.opencontainers.image.created=$CI_JOB_STARTED_AT"
      --label "org.opencontainers.image.revision=$CI_COMMIT_SHA"
      --label "org.opencontainers.image.version=$CI_COMMIT_REF_NAME"
      --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
      .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA


sast:
  stage: test_container
  image: docker:20
  rules:
    - when: always
  script:
    - docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker save -o demo-app.tar $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    - docker run --rm -e "WORKSPACE=${PWD}" -v $PWD:/app shiftleft/scan scan --src /app/demo-app.tar -o /app/reports --type docker
  artifacts:
    paths:
      - reports/*

trivy:
  stage: test_container
  image:
    name: aquasec/trivy:0.24.4
    entrypoint: [""]
  services:
    - docker:dind
  rules:
    - when: always
  script:
    - trivy --version
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy image --clear-cache
    # update vulnerabilities db
    - time trivy image --download-db-only
    # Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    - time trivy image --exit-code 0 --format template --template "@/contrib/gitlab.tpl"
        --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Prints full report
    - time trivy image --exit-code 0 $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    # Fail on critical vulnerabilities
    - time trivy image --exit-code 1 --severity CRITICAL $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
  cache:
    paths:
      - .trivycache/
  # Enables https://docs.gitlab.com/ee/user/application_security/container_scanning/ (Container Scanning report is available on GitLab EE Ultimate or GitLab.com Gold)
  artifacts:
    when: always
    reports:
      container_scanning: gl-container-scanning-report.json

merge_request_to_infra:
  stage: merge_request_to_infra
  image: ubuntu:20.04
  rules:
    - !reference [.merge_main, rules]
  script:
    - apt update
    - apt install -y git
    - apt install -y curl
    - curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    - chmod 700 get_helm.sh
    - ./get_helm.sh
    - ./git/make_merge_request.sh
